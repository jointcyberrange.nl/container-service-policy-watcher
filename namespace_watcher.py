import logging
import os
import sys
import yaml
import requests
import threading
import time

from kubernetes import client, config, watch


# Configure logging to output to stdout
logging.basicConfig(stream=sys.stdout, level=logging.INFO,
                    format='%(asctime)s %(levelname)s: %(message)s')

try:
    config.load_incluster_config()
except config.ConfigException:
    config.load_kubeconfig()

api = client.CoreV1Api()
networking = client.NetworkingV1Api()

# Define the URL of the network policy YAML
policy_url = "https://gitlab.com/jointcyberrange.nl/container-service-policy-watcher/-/raw/main/network-policy.yaml"

def fetch_policy_from_url(url):
    response = requests.get(policy_url)
    if response.status_code != 200:
        logging.error(f"Failed to fetch network policy YAML from URL: {policy_url}")
        sys.exit(1)
    return response.text
        
def create_or_update_policy(namespace_name, policy):
    try:
        networking.create_namespaced_network_policy(namespace=namespace_name, body=policy)
        logging.info(f"Network policy for namespace {namespace_name} created or updated successfully")
    except client.exceptions.ApiException as e:
        if e.status == 409 and 'already exists' in e.body:
            logging.info(f"Network policy for namespace {namespace_name} already exists, skipping")
        else:
            raise e
        
def delete_policy(namespace_name, policy_name):
    try:
        networking.delete_namespaced_network_policy(namespace=namespace_name, name=policy_name)
        logging.info(f"Network policy for namespace {namespace_name} deleted successfully")
    except client.exceptions.ApiException as e:
        if e.status == 404:
            logging.info(f"Network policy for namespace {namespace_name} not found, skipping deletion")
        else:
            raise e

def compare_policies(existing_policy, fetched_policy):
    return existing_policy.to_dict() != fetched_policy

def process_policy(namespace_name, policy):
    if 'chal' in namespace_name:
        try:
            existing_policy = networking.read_namespaced_network_policy(name=policy['metadata']['name'], namespace=namespace_name)
            if compare_policies(existing_policy, policy):
                logging.info(f"Updating network policy for namespace {namespace_name}")
                delete_policy(namespace_name, policy['metadata']['name'])
                create_or_update_policy(namespace_name, policy)
            else:
                logging.info(f"Network policy for namespace {namespace_name} is up to date, skipping")
        except client.exceptions.ApiException as e:
            if e.status == 404:
                create_or_update_policy(namespace_name, policy)
            else:
                raise e

def watch_and_update_policy():
    v1 = client.CoreV1Api()
    w = watch.Watch()
    for event in w.stream(v1.list_namespace, timeout_seconds=0):
        namespace_name = event['object'].metadata.name
        if event['type'] == 'ADDED':
            logging.info(f"Namespace {namespace_name} added")
            policy_yaml = fetch_policy_from_url(policy_url)
            policy = yaml.safe_load(policy_yaml)
            process_policy(namespace_name, policy)

def check_and_update_policy():
    current_policy = None
    while True:
        try:
            policy_yaml = fetch_policy_from_url(policy_url)
            if current_policy != policy_yaml:
                logging.info("Network policy YAML has changed. Updating existing policies...")
                current_policy = policy_yaml
                policy = yaml.safe_load(policy_yaml)
                namespaces = api.list_namespace().items
                for namespace in namespaces:
                    namespace_name = namespace.metadata.name
                    process_policy(namespace_name, policy)
        except ProtocolError:
            logging.error("Connection broken. Restarting the script...")
            # Restart the script using subprocess
            python_path = sys.executable
            script_path = os.path.abspath(__file__)
            subprocess.call([python_path, script_path])
            sys.exit(0)
        time.sleep(60)  # Sleep for 5 minutes (300 seconds)

# Start watching and updating the policy.
watch_thread = threading.Thread(target=watch_and_update_policy)
watch_thread.daemon = True
watch_thread.start()

# Check for policy changes and update existing policies.
check_and_update_policy()
